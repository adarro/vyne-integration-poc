## Links'n' stuff:

Elastic for logs:

http://3.8.176.203:5601/app/discover#/?_g=(filters:!(),refreshInterval:(pause:!t,value:0),time:(from:now-15m,to:now))&_a=(columns:!(level,appName,message),filters:!(),index:'54fdf000-fec6-11eb-9147-a776cd1acd5f',interval:auto,query:(language:kuery,query:''),savedQuery:Logs,sort:!(!('@timestamp',desc)))

Orchestrator config:

--vyne.pipeline.definitions.location=/home/marty/dev/vyne-demos/pipelines/pipeline-retail-sync/pipelines/pipelines.json --logstash.hostname=3.8.176.203

### Lenses UI:
http://127.0.0.1:3030/


### Date to use in queries
2021-08-17T09:00:00Z
2021-08-23T14:32:00Z

"2021-09-17T09:00:00Z"

## List transactions

```taxi
findAll { OrderTransaction[]( TransactionTime > "2021-08-17T09:00:00Z" ) } 
```


## List transactions, modify the SKU
```
import demo.CustomerEmailAddress

findAll { OrderTransaction[]( TransactionTime > "2021-08-17T09:00:00Z" ) } 
as {
    customer : CustomerEmailAddress
    transactionValue : TransactionPrice
    products: TransactionProductDescription -> {
        sku: ProductSku
        baseSKU : BaseSKU
        color : ProductColor 
        size : ProductSize 
    }[]
}[]
```

## List transactions, modify the SKU with function concat
```
import demo.CustomerEmailAddress

findAll { OrderTransaction[]( TransactionTime > "2021-08-17T09:00:00Z" ) } 
as {
    customer : CustomerEmailAddress
    transactionValue : TransactionPrice
    products: TransactionProductDescription -> {
        sku: ProductSku
        baseSKU : BaseSKU
        color : ProductColor 
        size : ProductSize 
        dashedSku : String by concat(this.baseSKU, '-', this.color, '-', this.size)
    }[]
}[]
```



## List transactions, reformatting the date
```
import demo.CustomerEmailAddress
findAll { OrderTransaction[]( TransactionTime > "2021-08-17T09:00:00Z" ) } 
as {
    transactionDate : TransactionTime(@format = 'dd-MMM-yy HH:mm:ss')
    customer : CustomerEmailAddress
    products: TransactionProductDescription -> {
        sku: ProductSku
        baseSKU : BaseSKU
        color : ProductColor 
        size : ProductSize 
    }[]
}[]
```

## List transactions, pulling in addition customer data
```
import demo.CustomerEmailAddress
findAll { OrderTransaction[]( TransactionTime > "2021-08-17T09:00:00Z" ) } 
as {
    transactionDate : TransactionTime(@format = 'dd-MMM-yy HH:mm:ss')
    customer : {
        email : CustomerEmailAddress
        firstName : CustomerFirstName
        lastName : CustomerLastName
    }
    products: TransactionProductDescription -> {
        sku: ProductSku
        baseSKU : BaseSKU
        color : ProductColor 
        size : ProductSize 
    }[]
}[]
```