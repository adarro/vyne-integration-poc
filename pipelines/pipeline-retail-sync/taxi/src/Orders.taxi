import demo.CustomerLastName
import demo.CustomerEmailAddress
import demo.CustomerFirstName

type ProductSku inherits String

[[ The base SKU for the product.  Doesn't include things such as size or color, but is the basis for derived SKU's  ]]
type BaseSKU inherits String


[[ A product, as defined in our Product service ]]
model Product {
    [[ The product decsription ]]
    description: ProductDescription inherits String
    [[ The base SKU for the product.  Doesn't include things such as size or color, but is the basis for derived SKU's  ]]
    baseSKU : BaseSKU inherits String
    [[ The color of the item for this SKU ]]
    color : ProductColor inherits String
    [[ The size of the item for this SKU ]]
    size : ProductSize inherits String
    [[ The SKU - the main way of identifying the product ]]
    @Id
    sku : ProductSku
    variantDescription: ProductVariantDescription inherits String
}

model TransactionProductDescription {
    sku: ProductSku
    description: ProductVariantDescription
}

model TransactionProduct {
    baseSku: BaseSKU
    description: ProductDescription
    size: ProductSize
    color: ProductColor
}

[[ Models transactions as they occur in our order tracking system ]]
model OrderTransaction {
    [[ The products included in this transcation  ]]
    products: TransactionProductDescription[]
    [[ The date and time of the transaction ]]
    time : TransactionTime inherits Instant
    [[ The email address of the purchasing customer ]]
    customer : CustomerEmailAddress
    [[ The value of the transaction ]]
    value : TransactionPrice inherits Decimal
}

model QueryResultType {
    customer : CustomerEmailAddress
    @FirstNotEmpty
    name : CustomerFirstName
    products: TransactionProductDescription -> {
        sku: ProductSku
        baseSku: BaseSKU
        description: ProductDescription
        size: ProductSize
        color: ProductColor 

        nestedSku: String by concat(this.baseSku, '-', this.size, '-', this.color)
    }[]
}

[[ Describes an update of stock levels from an order, into our stock management service

]]
model StockServiceOrderEvent {
    [[ The customers email address, used for identification ]]
    customer : CustomerEmailAddress
    [[ Customers first name]]
    firstName : CustomerFirstName
    [[ Customers last name]]
    lastName : CustomerLastName

    [[ A collection of items included in this order ]]
    products: TransactionProductDescription -> {
        baseSku: BaseSKU
        size: ProductSize
        color: ProductColor 
    }[]
}

service ProductService {
    @HttpOperation(method = "GET", url = "http://localhost:9650/products")
    operation listAllProducts():Product[]
    
    @HttpOperation(method = "GET", url = "http://localhost:9650/products/{sku}")
    operation findProduct(@PathVariable(name="sku") sku:ProductSku):Product
}

service OrderService {
    @HttpOperation(method = "GET", url="http://localhost:9650/orders/since/{since}")
    operation listOrders(@PathVariable(name="since") since:Instant):OrderTransaction[]( TransactionTime > since)
}

service StockService {
    @HttpOperation(method = "POST", url="http://localhost:9650/stock/orders")
    operation submitOrders(@RequestBody orders:StockServiceOrderEvent[])
}