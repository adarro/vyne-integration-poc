package io.vyne.demos.retail

import com.google.common.cache.CacheBuilder
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.scheduling.annotation.EnableScheduling
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RestController
import java.time.Instant

@SpringBootApplication
@EnableScheduling
open class RetailOrdersApp {
    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            SpringApplication.run(RetailOrdersApp::class.java, *args)
        }
    }
}

@RestController
class OrdersService {

    private val cache = CacheBuilder.newBuilder()
        .maximumSize(1000)
        .build<String, OrderTransaction>()


    @GetMapping("/products")
    fun listProducts():List<Product> = ProductGenerator.products

    @GetMapping("/products/{sku}")
    fun getProduct(@PathVariable("sku") sku:String):Product? = ProductGenerator.products.firstOrNull { it.SKU == sku }

    @GetMapping("/orders/since/{since}")
    fun getRecentOrders(@PathVariable("since") since: Instant): List<OrderTransaction> {
//        System.out.println("Received request for orders since $since")
//        return listOf("jimmy@demo.com", "steve@demo.com")
        return cache.asMap()
            .values
            .filter {
                it.time.isAfter(since)
            }
            .toList()
    }

    fun addOrder(orderTransaction: OrderTransaction) {
        cache.put(orderTransaction.id, orderTransaction)
    }
}