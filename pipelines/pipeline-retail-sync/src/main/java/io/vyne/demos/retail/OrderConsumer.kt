package io.vyne.demos.retail

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import mu.KotlinLogging
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController

data class OrderEventProduct(
    val baseSku: String,
    val size: String,
    val color: String
)

data class OrderSubmittedEvent(
    val customer: String,
    val firstName: String,
    val lastName: String,
    val products: List<OrderEventProduct>
)

@RestController
class OrderConsumer {
    private val logger = KotlinLogging.logger {}
    private val jacksonWriter = jacksonObjectMapper().writerWithDefaultPrettyPrinter()
    private var counter:Int = 0
    @PostMapping("/stock/orders")
    fun submitOrders(@RequestBody orders: List<OrderSubmittedEvent>) {
        counter++
//        if (counter % 5 == 0) {
//            throw RateLimitedException()
//        }
        logger.info { "Received a list of orders: \n${jacksonWriter.writeValueAsString(orders)}" }
    }
}

@ResponseStatus(HttpStatus.TOO_MANY_REQUESTS)
class RateLimitedException() : RuntimeException("Number of requests exceeded.  Try again later.")