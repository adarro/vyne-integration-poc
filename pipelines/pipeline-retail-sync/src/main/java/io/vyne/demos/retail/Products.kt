package io.vyne.demos.retail

import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RestController
import java.math.BigDecimal
import java.time.Instant
import java.util.UUID
import kotlin.random.Random

data class Product(
    val description: String,
    val baseSKU: String,
    val color: String,
    val size: String
) {
    val SKU = "$baseSKU${color.toUpperCase()}$size"
    val variantDescription = "$description $color $size"
}

object ProductGenerator {
    val baseProducts = mapOf(
        "Vyne Hoodie" to "W100080",
        "Vyne Sweat Pants" to "W100081",
        "Vyne Snuggle Onesy" to "W100082",
        "Vyne Hat" to "W100083",
        "Vyne Polo" to "W100084"
    )
    val colors = listOf("Grey", "Black", "Pink", "Blue", "Orange", "Burberry", "Navy")
    val sizes = listOf("XS", "S", "M", "L", "XL", "XXL")

    val products = baseProducts.flatMap { (description, baseSku) ->
        colors.flatMap { color ->
            sizes.map { size ->
                Product(description, baseSku, color, size)
            }
        }
    }
}

data class Customer(val emailAddress: String)

val customers = listOf(
    Customer(emailAddress = "jimmy@demo.com"),
    Customer(emailAddress = "steve@demo.com"),
    Customer(emailAddress = "peter@demo.com"),
    Customer(emailAddress = "andrew@demo.com"),
    Customer(emailAddress = "fran@demo.com"),
    Customer(emailAddress = "serhat@demo.com"),
    Customer(emailAddress = "mike@demo.com"),
    Customer(emailAddress = "marty@demo.com"),
    Customer(emailAddress = "teresa@demo.com"),
    Customer(emailAddress = "amy@demo.com"),
    Customer(emailAddress = "izzy@demo.com"),
    Customer(emailAddress = "charlie@demo.com"),
)

data class ProductDescription(val sku: String, val description: String)
data class OrderTransaction(
    val customer: String,
    val products: List<ProductDescription>,
    val time: Instant = Instant.now(),
) {
    val id = UUID.randomUUID().toString()
    val value: BigDecimal = BigDecimal.valueOf(products.size * 10.99)
}

@Component
@RestController
class Shopper(val ordersService: OrdersService) {

    private val logger = mu.KotlinLogging.logger {}
    private var shopperEnabled = false

    init {
        (0 until 500).forEach {
            generateTransactions(Instant.now().minusSeconds(Random.nextLong(0L, 86400)))
        }
    }

    @PostMapping("/shopper/start")
    fun startShopper() {
        shopperEnabled = true
        logger.info { "Starting the shoppers..." }
        generateTransactions()
    }

    @PostMapping("/shopper/stop")
    fun stopShopper() {
        logger.info { "Stopping the shoppers..." }
        shopperEnabled = false
    }

    @Scheduled(fixedDelay = 10_000) // evey 10 seconds
    fun generateTransactionsIfEnabled() {
        if (shopperEnabled) {
            generateTransactions()
        }
    }


    fun generateTransactions(time: Instant = Instant.now()) {
        val numberOfTransactions = Random.nextInt(1, 10)
        repeat((1 until numberOfTransactions).count()) {
            val customer = customers.random()
            val numberOfProducts = Random.nextInt(1, 6)
            val products = (0 until numberOfTransactions).map {
                ProductGenerator.products.random()
            }
            val orderTransaction = OrderTransaction(
                customer.emailAddress,
                products.map { ProductDescription(it.SKU, it.variantDescription) },
                time
            )

            logger.info { "New transaction: \n$orderTransaction" }
            ordersService.addOrder(orderTransaction)
        }
    }
}