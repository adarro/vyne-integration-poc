package io.vyne.demos.rewards.common

import lang.taxi.annotations.DataType
import java.math.BigDecimal


@DataType("demo.RewardsAccountBalance")
data class RewardsAccountBalance(
        val cardNumber: RewardsCardNumber,
        val balance: RewardsBalance,
        // TODO: https://gitlab.com/vyne/vyne/issues/39
        val currencyUnit: CurrencyUnit
)

//@DataType("demo.CurrencyUnit")
//typealias CurrencyUnit = String

// TODO : https://gitlab.com/vyne/vyne/issues/39
@DataType("demo.CurrencyUnit")
enum class CurrencyUnit {
    POINTS,
    GBP
}

@DataType("demo.RewardsCardNumber")
typealias RewardsCardNumber = String

@DataType("demo.RewardsBalance")
typealias RewardsBalance = BigDecimal

@DataType("demo.CustomerName")
typealias CustomerName = String

@DataType("demo.CustomerFirstName")
typealias CustomerFirstName = String

@DataType("demo.CustomerLastName")
typealias CustomerLastName = String

@DataType("demo.Postcode")
typealias Postcode = String