package io.vyne.demos.marketing.promotions


import io.vyne.spring.VyneSchemaPublisher
import lang.taxi.annotations.DataType
import lang.taxi.generators.kotlin.TypeAliasRegister
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.cloud.netflix.eureka.EnableEurekaClient
import java.time.LocalDate

@SpringBootApplication
@VyneSchemaPublisher
@EnableEurekaClient
open class PromotionServiceApp {
    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            TypeAliasRegister.registerPackage("io.vyne.demos.rewards")
            SpringApplication.run(PromotionServiceApp::class.java, *args)
        }
    }
}

@DataType("demos.Promotion")
interface Promotion {
    val title: String
    val availableUntil: LocalDate
}
