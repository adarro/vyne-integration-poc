package io.vyne.demos.marketing.promotions

import io.vyne.demos.rewards.common.CustomerName
import io.vyne.demos.rewards.common.RewardsBalance
import lang.taxi.annotations.DataType
import lang.taxi.annotations.Operation
import lang.taxi.annotations.ParameterType
import lang.taxi.annotations.Service
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController
import java.time.LocalDate

@RestController
@Service
class DiscountPromotionService {

    @PostMapping("/discounts")
    @Operation
    fun getDiscountPromotion(@RequestBody clientDetails: PromotionClientDetails): DiscountPromotion {
        return DiscountPromotion("Hey, ${clientDetails.clientName}, get 20% off today!  Hurry, offer ends soon")
    }

    @PostMapping("/gift")
    @Operation
    fun getDoublePointsPromotion(@RequestBody request: ClientRewardsInfo): DoublePointsPromotion {
        return DoublePointsPromotion(
                message = "Hey, ${request.clientName}, you could double your points - that's an extra ${request.rewardsBalance} points, just by shopping at one of these stores:",
                shops = listOf("Twin Pines", "Lone Pines", "Twin Peaks")
        )
    }

//    @PostMapping("/soup/{postcode}")
//    @Operation
//    fun getSoupForPostcode(@PathVariable("postcode") postcode: Postcode): SoupPromotion {
//        return if (postcode.startsWith("SW")) {
//            SoupPromotion("Soup for you!")
//        } else {
//            SoupPromotion("No soup for you.")
//        }
//    }
}


data class SoupPromotion(val message: String) : Promotion {
    override val title: String = "Soup from our Kitchen!"
    override val availableUntil: LocalDate = LocalDate.now().plusDays(2)
}

@DataType("demos.DiscountPromotion")
data class DiscountPromotion(val message: String) : Promotion {
    override val title: String = "AwesomeDiscount"
    override val availableUntil: LocalDate = LocalDate.now().plusDays(2)

}

@ParameterType
@DataType("demo.PromotionClientDetails")
data class PromotionClientDetails(
        val clientName: CustomerName
)

@DataType("demo.ClientRewardsInfo")
@ParameterType
data class ClientRewardsInfo(
        val clientName: CustomerName,
        val rewardsBalance: RewardsBalance
)

@DataType("demo.DoublePointsPromotion")
data class DoublePointsPromotion(
        val message: String,
        val shops: List<String>
) : Promotion {
    override val title: String = "EarnDoublePoints"
    override val availableUntil: LocalDate = LocalDate.now().plusDays(6)

}