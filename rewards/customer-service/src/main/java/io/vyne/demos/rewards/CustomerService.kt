package io.vyne.demos.rewards

import io.vyne.demos.rewards.common.CustomerFirstName
import io.vyne.demos.rewards.common.CustomerLastName
import io.vyne.demos.rewards.common.Postcode
import io.vyne.spring.VyneSchemaPublisher
import lang.taxi.annotations.DataType
import lang.taxi.annotations.Operation
import lang.taxi.annotations.Service
import lang.taxi.generators.kotlin.TypeAliasRegister
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.cloud.netflix.eureka.EnableEurekaClient
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RestController


@SpringBootApplication
@EnableEurekaClient
@VyneSchemaPublisher
open class CustomerServiceApp {
    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            TypeAliasRegister.registerPackage("io.vyne.demos.rewards")
            SpringApplication.run(CustomerServiceApp::class.java, *args)
        }
    }
}

@RestController
@Service(documentation = "Provides access to customer information, such as Customer id's, names, and email addresses")
class CustomerService {
    private val customers = listOf(
        Customer(1, "Jimmy", "Schmitt", "jimmy@demo.com", "SW11"),
        Customer(2, "Peter", "Piper", "peter@demo.com", "NE3"),
        Customer(3, "Steve", "McQueen", "steve@demo.com", "N4"),
        Customer(4, "Andrew", "McPatterson", "andrew@demo.com", "N4"),
        Customer(5, "Fran", "Fraggle", "fran@demo.com", "N4"),
        Customer(6, "Serhat", "Thesir", "serhat@demo.com", "N4"),
        Customer(7, "Mike", "McGee", "mike@demo.com", "N4"),
        Customer(8, "Marty", "McPitt", "marty@demo.com", "N4"),
        Customer(9, "Teresa", "McPitt", "teresa@demo.com", "N4"),
        Customer(10, "Amy", "LeGuide", "amy@demo.com", "N4"),
        Customer(11, "Izzy", "McMini", "izzy@demo.com", "N4"),
        Customer(12, "Charlie", "McMini", "charlie@demo.com", "N4"),


        )

    @Operation(documentation = "Looks up a customer record, using their email address")
    @GetMapping("/customers/email/{email}")
    fun getCustomerByEmail(@PathVariable("email") customerEmail: CustomerEmailAddress): Customer {
        return customers.first { it.email == customerEmail }
    }

    @Operation(documentation = "Looks up a customer Id, from their email address")
    @GetMapping("/customers/{email}/id")
    fun getCustomerIdByEmail(@PathVariable("email") customerEmail: CustomerEmailAddress): CustomerId {
        return customers.first { it.email == customerEmail }.id
    }

    @Operation(documentation = "Looks up a customer by their Customer Id")
    @GetMapping("/customers/{id}")
    fun getCustomer(@PathVariable("id") customerId: CustomerId): Customer {
        return customers.first { it.id == customerId }
    }

    @Operation(documentation = "Lists all customers")
    @GetMapping("/customers")
    fun getCustomers(): List<Customer> {
        return customers
    }

    // If you're following the tutorial (https://docs.vyne.co/examples/example-rewards-cards)
    // then uncomment this section to explore with introducing breaking changes to the API.
//    @Operation
//    @PostMapping("/customers/identityQuery")
//    fun findCustomer(@RequestBody identity: Identity): Customer {
//        return customers.first { it.contactInfo.emailAddress == identity.emailAddress }
//    }

}

@DataType("demo.CustomerId")
typealias CustomerId = Int

@DataType("demo.CustomerEmailAddress")
typealias CustomerEmailAddress = String

@DataType("demo.Customer")
data class Customer(
    val id: CustomerId,
    val firstName: CustomerFirstName,
    val lastName: CustomerLastName,
    val email: CustomerEmailAddress,
    val postcode: Postcode
)

// If you're following the tutorial (https://docs.vyne.co/examples/example-rewards-cards)
// then uncomment this section to explore with introducing breaking changes to the API.

//
//@DataType("demo.Customer")
//data class Customer(
//        val id: CustomerId,
//        val name: String,
//        val postcode: Postcode,
//        val contactInfo: ContactInfo
//) {
//    constructor(id: Int, name: String, email: String, postcode: Postcode) : this(id, name, postcode, ContactInfo("555-234-2003", email))
//}
//
//data class ContactInfo(
//        val phoneNumber: String,
//        val emailAddress: CustomerEmailAddress
//)
//
//@DataType
//@ParameterType
//data class Identity(val emailAddress: CustomerEmailAddress)
//
