package io.vyne.demos.marketing



import io.vyne.spring.VyneSchemaPublisher
import lang.taxi.annotations.DataType
import lang.taxi.annotations.Operation
import lang.taxi.annotations.Service
import lang.taxi.generators.kotlin.TypeAliasRegister
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.cloud.netflix.eureka.EnableEurekaClient
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RestController


@SpringBootApplication
@VyneSchemaPublisher
@EnableEurekaClient
open class MarketingServiceApp {
    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            TypeAliasRegister.registerPackage("io.vyne.demos.rewards")
            TypeAliasRegister.registerPackage("io.vyne.demos.marketing")
            SpringApplication.run(MarketingServiceApp::class.java, *args)
        }
    }
}


@RestController
@Service(documentation = "Provides access to card data, such as card numbers")
class MarketingService {
    val customerRecords = listOf(
            CustomerMarketingRecord(1, "4005-2003-2330-1002"),
            CustomerMarketingRecord(2, "4002-0230-9979-2004"),
            CustomerMarketingRecord(3, "4974-2847-2994-2003")
    ).associateBy { it.id }

    @GetMapping("/marketing/{customerId}")
    @Operation(documentation = "Looks up a customer's card data by their customer Id")
    fun getMarketingDetailsForCustomer(@PathVariable("customerId") customerId: CustomerId): CustomerMarketingRecord {
        return customerRecords[customerId]
                ?: throw error("No customer marketing record found for customer $customerId")
    }
}

@DataType("demo.CustomerId")
typealias CustomerId = Int

@DataType("demo.RewardsCardNumber")
typealias RewardsCardNumber = String

@DataType
data class CustomerMarketingRecord(
        val id: CustomerId,
        val rewardsCardNumber: RewardsCardNumber
)
