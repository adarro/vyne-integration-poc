package io.vyne.demos.rewards.facade;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 11}, bv = {1, 0, 2}, k = 1, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010$\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010 \n\u0002\b\u0002\b\u0007\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J$\u0010\u0007\u001a\u0014\u0012\b\u0012\u00060\tj\u0002`\n\u0012\u0006\u0012\u0004\u0018\u00010\u00010\b2\b\b\u0001\u0010\u000b\u001a\u00020\tH\u0007J\u0017\u0010\f\u001a\u00020\r2\b\b\u0001\u0010\u000b\u001a\u00020\u000eH\u0007\u00a2\u0006\u0002\u0010\u000fJ\u001d\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u000e0\u00112\b\b\u0001\u0010\u000b\u001a\u00020\u000eH\u0007\u00a2\u0006\u0002\u0010\u0012R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006\u00a8\u0006\u0013"}, d2 = {"Lio/vyne/demos/rewards/facade/QueryController;", "", "vyne", "Lio/vyne/VyneClient;", "(Lio/vyne/VyneClient;)V", "getVyne", "()Lio/vyne/VyneClient;", "getCustomerNameByEmailAddress", "", "", "Lio/vyne/TypeName;", "email", "getCustomerPointsBalance", "Ljava/math/BigDecimal;", "error/NonExistentClass", "(Lerror/NonExistentClass;)Ljava/math/BigDecimal;", "getPromotions", "", "(Lerror/NonExistentClass;)Ljava/util/List;", "query-facade"})
@org.springframework.web.bind.annotation.RestController()
public final class QueryController {
    @org.jetbrains.annotations.NotNull()
    private final io.vyne.VyneClient vyne = null;
    
    @org.jetbrains.annotations.NotNull()
    @org.springframework.web.bind.annotation.GetMapping(value = {"/customers/{customerEmail}"})
    public final java.util.Map<java.lang.String, java.lang.Object> getCustomerNameByEmailAddress(@org.jetbrains.annotations.NotNull()
    @org.springframework.web.bind.annotation.PathVariable(value = "customerEmail")
    java.lang.String email) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @org.springframework.web.bind.annotation.GetMapping(value = {"/balances/{customerEmail}"})
    public final java.math.BigDecimal getCustomerPointsBalance(@org.jetbrains.annotations.NotNull()
    @org.springframework.web.bind.annotation.PathVariable(value = "customerEmail")
    error.NonExistentClass email) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @org.springframework.web.bind.annotation.GetMapping(value = {"/promotions/{customerEmail}"})
    public final java.util.List<error.NonExistentClass> getPromotions(@org.jetbrains.annotations.NotNull()
    @org.springframework.web.bind.annotation.PathVariable(value = "customerEmail")
    error.NonExistentClass email) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final io.vyne.VyneClient getVyne() {
        return null;
    }
    
    public QueryController(@org.jetbrains.annotations.NotNull()
    io.vyne.VyneClient vyne) {
        super();
    }
}