package io.vyne.demos.rewards.facade;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 11}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\n\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0087\b\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u000e\u0010\b\u001a\u00020\u0003H\u00c6\u0003\u00a2\u0006\u0002\u0010\u0006J\u0018\u0010\t\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u0003H\u00c6\u0001\u00a2\u0006\u0002\u0010\nJ\u0013\u0010\u000b\u001a\u00020\f2\b\u0010\r\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u000e\u001a\u00020\u000fH\u00d6\u0001J\t\u0010\u0010\u001a\u00020\u0011H\u00d6\u0001R\u0013\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\n\n\u0002\u0010\u0007\u001a\u0004\b\u0005\u0010\u0006\u00a8\u0006\u0012"}, d2 = {"Lio/vyne/demos/rewards/facade/Customer;", "", "customerEmail", "error/NonExistentClass", "(Lerror/NonExistentClass;)V", "getCustomerEmail", "()Lerror/NonExistentClass;", "Lerror/NonExistentClass;", "component1", "copy", "(Lerror/NonExistentClass;)Lio/vyne/demos/rewards/facade/Customer;", "equals", "", "other", "hashCode", "", "toString", "", "query-facade"})
@lang.taxi.annotations.DataType()
public final class Customer {
    @org.jetbrains.annotations.NotNull()
    private final error.NonExistentClass customerEmail = null;
    
    @org.jetbrains.annotations.NotNull()
    public final error.NonExistentClass getCustomerEmail() {
        return null;
    }
    
    public Customer(@org.jetbrains.annotations.NotNull()
    error.NonExistentClass customerEmail) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final error.NonExistentClass component1() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final io.vyne.demos.rewards.facade.Customer copy(@org.jetbrains.annotations.NotNull()
    error.NonExistentClass customerEmail) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public java.lang.String toString() {
        return null;
    }
    
    @java.lang.Override()
    public int hashCode() {
        return 0;
    }
    
    @java.lang.Override()
    public boolean equals(@org.jetbrains.annotations.Nullable()
    java.lang.Object p0) {
        return false;
    }
}