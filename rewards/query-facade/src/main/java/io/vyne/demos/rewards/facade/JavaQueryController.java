package io.vyne.demos.rewards.facade;

import io.vyne.VyneClient;
import io.vyne.demos.rewards.common.RewardsAccountBalance;
import io.vyne.query.Fact;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;

@RestController
public class JavaQueryController {
    private VyneClient vyne;

    public JavaQueryController(VyneClient vyne) {
        this.vyne = vyne;
    }

    @GetMapping("/balances/java/{customerEmail}")
    BigDecimal getCustomerPointsBalance(@PathVariable("customerEmail") String customerEmailAddress) {
        RewardsAccountBalance balance = vyne.given(new Fact("demo.CustomerEmailAddress", customerEmailAddress))
                .discover(RewardsAccountBalance.class);
        return balance.getBalance();
    }


}
