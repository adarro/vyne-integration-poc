package io.vyne.demos.rewards.facade


import io.vyne.EnableVyneClient
import io.vyne.TypeName
import io.vyne.VyneClient
import io.vyne.demos.marketing.promotions.Promotion
import io.vyne.demos.rewards.CustomerEmailAddress
import io.vyne.demos.rewards.common.RewardsAccountBalance
import io.vyne.query.asFact
import lang.taxi.TypeAliasRegistry
import lang.taxi.annotations.DataType
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.cloud.netflix.eureka.EnableEurekaClient
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RestController
import java.math.BigDecimal

@SpringBootApplication
@EnableEurekaClient
@EnableVyneClient
open class RewardsQueryFacadeApp {
    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
//            TypeAliasRegistry.register(
//                    io.vyne.demos.rewards.TypeAliases::class)
            SpringApplication.run(RewardsQueryFacadeApp::class.java, *args)
        }
    }
}




@RestController
class QueryController(val vyne: VyneClient) {

    @GetMapping("/customers/{customerEmail}")
    fun getCustomerNameByEmailAddress(@PathVariable("customerEmail") email: String): Map<TypeName, Any?> {
        return vyne.given(email.asFact("demo.CustomerEmailAddress"))
                .discover("demo.CustomerName")
    }
















    @GetMapping("/balances/{customerEmail}")
    fun getCustomerPointsBalance(@PathVariable("customerEmail") email: CustomerEmailAddress): BigDecimal {

        val accountBalance = vyne.given(Customer(email))
                .discover<RewardsAccountBalance>()!!
        return accountBalance.balance
    }

    @GetMapping("/promotions/{customerEmail}")
    fun getPromotions(@PathVariable("customerEmail") email: CustomerEmailAddress): List<Promotion> {
        return vyne.given(email.asFact("demo.CustomerEmailAddress"))
                .gather<Promotion>()
    }
}

@DataType
data class Customer(val customerEmail: CustomerEmailAddress)