package io.vyne.demos.marketing.shop


import io.vyne.demos.rewards.common.CurrencyUnit
import io.vyne.demos.rewards.common.RewardsAccountBalance
import io.vyne.demos.rewards.common.RewardsBalance
import io.vyne.spring.VyneSchemaPublisher
import lang.taxi.annotations.Constraint
import lang.taxi.annotations.Operation
import lang.taxi.annotations.Parameter
import lang.taxi.annotations.ParameterType
import lang.taxi.annotations.Service
import lang.taxi.generators.kotlin.TypeAliasRegister
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.cloud.netflix.eureka.EnableEurekaClient
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController

@SpringBootApplication
@EnableEurekaClient
@VyneSchemaPublisher
open class RewardsShopApp {
    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            TypeAliasRegister.registerPackage("io.vyne.demos.marketing")
            SpringApplication.run(RewardsShopApp::class.java, *args)
        }
    }
}

@Service(documentation = "Services for finding available rewards and their prices")
@RestController
class RewardsShopService {

    private val rewards = listOf(
            Reward("Weekend at the spa", 300, 1200),
            Reward("Night at the movies", 20, 200),
            Reward("Bottle of wine", 10, 100)
    )

//
/*    @GetMapping("/availableRewards/{pointsBalance}")
    @Operation(documentation = "Lists the available rewards for a given account balance")
    fun getAvailableRewards(@PathVariable("pointsBalance") balance: RewardsBalance): AvailableRewards {
        return AvailableRewards(
                this.rewards.filter { it.priceInPoints < balance.toInt() }
        )
    }*/
////
//
//    @PostMapping("/shop")
//    @Operation(documentation = "Lists the available rewards for a given account balance")
//    fun getAvailableRewards(@RequestBody balance: RewardsAccountBalance): AvailableRewards {
//        return AvailableRewards(
//                this.rewards.filter { it.priceInGbp < balance.balance.toInt() }
//        )
//    }
//
    @PostMapping("/shop")
    @Operation(documentation = "Lists the available rewards for a given account balance")
    fun getAvailableRewards(@RequestBody @Parameter(constraints = [Constraint("this.currencyUnit = 'GBP'")]) balance: RewardsAccountBalance): AvailableRewards {
        return AvailableRewards(
                this.rewards.filter { it.priceInGbp < balance.balance.toInt() }
        )
    }
}


data class AvailableRewards(val rewards: List<Reward>)
data class Reward(val name: String, val priceInGbp: Int, val priceInPoints: Int)

@ParameterType
data class SpendingBalance(
    val amount: RewardsBalance,
    // TODO: https://gitlab.com/vyne/vyne/issues/39
    val unit: CurrencyUnit
)

object ThingThatStopsImportsGettingTidiedAwayWhichReallyHurtsMyDemos {
    val constraint : Constraint? = null
    val parameter:Parameter? = null
    val postMapping:PostMapping? = null
    val getMapping:GetMapping? = null
    val requestBody:RequestBody? = null
    val operation:Operation? = null
    val pathVariable:PathVariable? = null
    val rewardsAccountBalance:RewardsAccountBalance? = null
}