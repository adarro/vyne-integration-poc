package io.vyne.demos.rewards.balances;

import io.vyne.spring.VyneSchemaPublisher;
import lang.taxi.generators.kotlin.TypeAliasRegister;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@VyneSchemaPublisher
@EnableEurekaClient
public class RewardsBalanceApp {
    public static void main(String[] args) {
        TypeAliasRegister.registerPackage("io.vyne.demos.rewards");
        SpringApplication.run(RewardsBalanceApp.class, args);
    }
}
