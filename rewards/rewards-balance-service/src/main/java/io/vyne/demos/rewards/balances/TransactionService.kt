package io.vyne.demos.rewards.balances

import io.vyne.demos.rewards.common.CurrencyUnit
import io.vyne.demos.rewards.common.RewardsAccountBalance
import io.vyne.demos.rewards.common.RewardsCardNumber
import lang.taxi.annotations.Operation
import lang.taxi.annotations.Service
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RestController
import java.math.BigDecimal

@RestController
@Service(documentation = "Provides access to card balances and transactions")
class TransactionService {
    val balances = listOf(
            RewardsAccountBalance("4005-2003-2330-1002", BigDecimal("2300"), CurrencyUnit.POINTS),
            RewardsAccountBalance("4002-0230-9979-2004", BigDecimal("2000"), CurrencyUnit.POINTS),
            RewardsAccountBalance("4974-2847-2994-2003", BigDecimal("10020"), CurrencyUnit.POINTS)
    ).associateBy { it.cardNumber }

    @GetMapping("/balances/{cardNumber}")
    @Operation(documentation = "Looks up the balance for a card.  Balances are returned in rewards points")
    fun getRewardsBalance(@PathVariable("cardNumber") cardNumber: RewardsCardNumber): RewardsAccountBalance {
        return balances[cardNumber] ?: throw error("No account with card number $cardNumber found")
    }

//    @PostMapping("/balances/{targetCurrency}")
//    @ResponseContract(basedOn = "source",
//            constraints = [ResponseConstraint("this.currencyUnit = targetCurrency")]
//    )
//    @Operation(documentation = "Converts from one currency to another.")
//    fun convert(@PathVariable("targetCurrency") targetCurrency: CurrencyUnit, @RequestBody source: RewardsAccountBalance): RewardsAccountBalance {
//        // This is obviously not a real conversion service.
//        val rate = 0.5.toBigDecimal()
//        return RewardsAccountBalance(
//                source.cardNumber,
//                source.balance.multiply(rate).setScale(0, RoundingMode.HALF_EVEN),
//                targetCurrency
//        )
//    }


}
