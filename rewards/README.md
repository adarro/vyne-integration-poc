# Customer Loyalty

This demo explores discovering the points balance for a customer's fictional loyalty card.

It's intended as a hello-world type demo, which shows off the basic connectivity capabilities of Vyne.

## Service Topology

We'll create three seperate services, none of which really work together:

 * Customer Service - The store of our customers.
 * Marketing Service - Provides a lookup of customer # to the loyalty card #
 * Loyalty Balance Service - Exposes the current balance for a given card #
 
