import com.starblicks.TypeNames.Order
import lang.taxi.annotations.DataType

@DataType(
  value = Order,
  imported = true
)
open class Order(
  val quantity: OrderQuantity,
  val price: OrderPrice
)
