import com.starblicks.TypeNames.OrderQuantity
import kotlin.Int
import lang.taxi.annotations.DataType

@DataType(
  value = OrderQuantity,
  imported = true
)
typealias OrderQuantity = Int
