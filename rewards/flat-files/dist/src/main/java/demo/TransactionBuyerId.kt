package demo

import com.starblicks.TypeNames.demo.TransactionBuyerId
import lang.taxi.annotations.DataType

@DataType(
  value = TransactionBuyerId,
  imported = true
)
typealias TransactionBuyerId = CustomerId
