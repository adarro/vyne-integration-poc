package demo

import com.starblicks.TypeNames.demo.RewardsCardNumber
import kotlin.String
import lang.taxi.annotations.DataType

@DataType(
  value = RewardsCardNumber,
  imported = true
)
typealias RewardsCardNumber = String
