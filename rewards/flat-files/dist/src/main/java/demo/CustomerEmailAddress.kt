package demo

import com.starblicks.TypeNames.demo.CustomerEmailAddress
import kotlin.String
import lang.taxi.annotations.DataType

@DataType(
  value = CustomerEmailAddress,
  imported = true
)
typealias CustomerEmailAddress = String
