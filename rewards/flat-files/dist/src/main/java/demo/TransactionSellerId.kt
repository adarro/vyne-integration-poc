package demo

import com.starblicks.TypeNames.demo.TransactionSellerId
import lang.taxi.annotations.DataType

@DataType(
  value = TransactionSellerId,
  imported = true
)
typealias TransactionSellerId = CustomerId
