package demo

import com.starblicks.TypeNames.demo.FirstName
import kotlin.String
import lang.taxi.annotations.DataType

@DataType(
  value = FirstName,
  imported = true
)
typealias FirstName = String
