package demo

import com.starblicks.TypeNames.demo.RewardsBalance
import java.math.BigDecimal
import lang.taxi.annotations.DataType

@DataType(
  value = RewardsBalance,
  imported = true
)
typealias RewardsBalance = BigDecimal
