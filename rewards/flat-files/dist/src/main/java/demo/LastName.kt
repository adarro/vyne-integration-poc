package demo

import com.starblicks.TypeNames.demo.LastName
import kotlin.String
import lang.taxi.annotations.DataType

@DataType(
  value = LastName,
  imported = true
)
typealias LastName = String
