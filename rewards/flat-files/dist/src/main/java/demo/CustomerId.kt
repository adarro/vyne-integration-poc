package demo

import com.starblicks.TypeNames.demo.CustomerId
import kotlin.String
import lang.taxi.annotations.DataType

@DataType(
  value = CustomerId,
  imported = true
)
typealias CustomerId = String
