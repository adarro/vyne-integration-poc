package demo

import com.starblicks.TypeNames.demo.CustomerName
import kotlin.String
import lang.taxi.annotations.DataType

@DataType(
  value = CustomerName,
  imported = true
)
typealias CustomerName = String
