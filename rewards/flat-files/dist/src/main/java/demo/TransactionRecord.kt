package demo

import com.starblicks.TypeNames.demo.TransactionRecord
import lang.taxi.annotations.DataType

@DataType(
  value = TransactionRecord,
  imported = true
)
open class TransactionRecord(
  val customerEmail: CustomerEmailAddress?,
  val customerId: CustomerId?,
  val cardNumber: RewardsCardNumber?
)
