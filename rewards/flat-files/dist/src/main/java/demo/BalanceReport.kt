package demo

import com.starblicks.TypeNames.demo.BalanceReport
import lang.taxi.annotations.DataType

@DataType(
  value = BalanceReport,
  imported = true
)
open class BalanceReport(
  val customerName: CustomerName,
  val customerEmail: CustomerEmailAddress,
  val customerId: CustomerId,
  val cardNumber: RewardsCardNumber,
  val balance: RewardsBalance
)
