package demo

import com.starblicks.TypeNames.demo.LastTransactionDate
import java.time.Instant
import lang.taxi.annotations.DataType

@DataType(
  value = LastTransactionDate,
  imported = true
)
typealias LastTransactionDate = Instant
