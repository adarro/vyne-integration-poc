package demo

import com.starblicks.TypeNames.demo.FullName
import kotlin.String
import lang.taxi.annotations.DataType

@DataType(
  value = FullName,
  imported = true
)
typealias FullName = String
