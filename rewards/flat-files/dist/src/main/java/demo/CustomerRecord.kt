package demo

import com.starblicks.TypeNames.demo.CustomerRecord
import lang.taxi.annotations.DataType

@DataType(
  value = CustomerRecord,
  imported = true
)
open class CustomerRecord(
  val id: CustomerId,
  val firstName: FirstName,
  val lastName: LastName,
  val emailAdress: CustomerEmailAddress,
  val fullName: FullName
)
