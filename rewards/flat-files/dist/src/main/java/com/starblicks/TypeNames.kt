package com.starblicks

import kotlin.String

object TypeNames {
  const val OrderQuantity: String = "OrderQuantity"

  const val OrderPrice: String = "OrderPrice"

  const val Order: String = "Order"

  const val OutputOrder: String = "OutputOrder"

  object demo {
    const val CustomerName: String = "demo.CustomerName"

    const val CustomerId: String = "demo.CustomerId"

    const val CustomerEmailAddress: String = "demo.CustomerEmailAddress"

    const val RewardsCardNumber: String = "demo.RewardsCardNumber"

    const val RewardsBalance: String = "demo.RewardsBalance"

    const val LastTransactionDate: String = "demo.LastTransactionDate"

    const val FirstName: String = "demo.FirstName"

    const val LastName: String = "demo.LastName"

    const val FullName: String = "demo.FullName"

    const val CustomerRecord: String = "demo.CustomerRecord"

    const val TransactionBuyerId: String = "demo.TransactionBuyerId"

    const val TransactionSellerId: String = "demo.TransactionSellerId"

    const val TransactionRecord: String = "demo.TransactionRecord"

    const val BalanceReport: String = "demo.BalanceReport"
  }
}
