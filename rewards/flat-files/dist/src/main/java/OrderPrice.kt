import com.starblicks.TypeNames.OrderPrice
import java.math.BigDecimal
import lang.taxi.annotations.DataType

@DataType(
  value = OrderPrice,
  imported = true
)
typealias OrderPrice = BigDecimal
