import com.starblicks.TypeNames.OutputOrder
import java.math.BigDecimal
import lang.taxi.annotations.DataType

@DataType(
  value = OutputOrder,
  imported = true
)
open class OutputOrder(
  val quantity: OrderQuantity,
  val price: OrderPrice,
  val averagePrice: BigDecimal
)
