namespace demo
[[
The first name of a customer.  Call them this, they love it.
]]
type CustomerName inherits String
type CustomerFirstName inherits String
type CustomerLastName inherits String

type CustomerId inherits Int
@Gdpr
type CustomerEmailAddress inherits String

type RewardsCardNumber inherits String
type RewardsBalance inherits Decimal
type LastTransactionDate inherits Instant

type FirstName inherits String
type LastName inherits String
type FullName inherits String


type CustomerId inherits String
type TransactionBuyerId inherits CustomerId
type TransactionSellerId inherits CustomerId

model CustomerDataSyncRequest {
    email : CustomerEmailAddress
}

service OrdersService {
    @HttpOperation(url = "http://localhost:9650/orders/since/{orderTime}", method = "GET")
    operation listRecentOrders(@PathVariable(name = "orderTime") orderTime: Instant):CustomerEmailAddress[]
}

[[
This models the output required for a specific platform
]]
model TransactionRecord {
    [[
    The preferred email address of the customer.
    Use this for all primary correspondence
    ]]
    customerEmail : CustomerEmailAddress? by column("email")
    @Gdpr
    customerId : CustomerId? by column("id")
    cardNumber : RewardsCardNumber? by column("cardNumber")
}

model BalanceReport {
    @FirstNotEmpty
    firstName : CustomerFirstName
    @FirstNotEmpty
    lastName: CustomerLastName
    @FirstNotEmpty
    email : CustomerEmailAddress
    @FirstNotEmpty
    customerId : CustomerId
    @FirstNotEmpty
    cardNumber : RewardsCardNumber
    @FirstNotEmpty
    balance: RewardsBalance
}
