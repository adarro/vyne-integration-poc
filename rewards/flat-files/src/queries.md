```
find { demo.TransactionRecord[] }
```

```
find { demo.TransactionRecord[] } as {
    @FirstNotEmpty
    customerEmail : CustomerEmailAddress
    @FirstNotEmpty
    customerId : CustomerId
    @FirstNotEmpty
    cardNumber : RewardsCardNumber
}[]

```

```
find { demo.TransactionRecord[] } as {
    @FirstNotEmpty
    firstName : CustomerFirstName
    @FirstNotEmpty
    lastName: CustomerLastName
    @FirstNotEmpty
    customerEmail : CustomerEmailAddress
    @FirstNotEmpty
    customerId : CustomerId
    @FirstNotEmpty
    cardNumber : RewardsCardNumber
    @FirstNotEmpty
    balance: RewardsBalance
}[]
```