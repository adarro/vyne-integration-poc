package io.vyne.demos.servicePublisher

import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@Api(value = "Sample Service Publisher Controller", description = "Exposes a single function accessible from Vyne")
class SampleController {
    @ApiOperation(value = "Get All Acme Entities", produces = "application/json")
    @GetMapping("/sample-service-publisher/v1/acme/entities")
    fun getAllAcmeEntities(): List<AcmeEntity> {
        return listOf(AcmeEntity("John Doe"), AcmeEntity("Marty Lee"), AcmeEntity("Jimmy Perez"))
    }
}

data class AcmeEntity(val name: String)