package io.vyne.demos.servicePublisher

import io.vyne.VersionedTypeReference
import io.vyne.spring.VyneSchemaPublisher
import io.vyne.spring.config.VyneSpringHazelcastConfiguration
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.context.annotation.Bean
import springfox.documentation.builders.PathSelectors
import springfox.documentation.builders.RequestHandlerSelectors
import springfox.documentation.spi.DocumentationType
import springfox.documentation.spring.web.plugins.Docket
import springfox.documentation.swagger2.annotations.EnableSwagger2


@SpringBootApplication
@EnableSwagger2
@VyneSchemaPublisher(schemaFile = "sample-service.taxi")
@EnableConfigurationProperties(VyneSpringHazelcastConfiguration::class)
class Bootstrapper {
    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            val app = SpringApplication(Bootstrapper::class.java)
            app.run(*args)
        }
        @Bean
        fun api(): Docket {
            return Docket(DocumentationType.SWAGGER_2)
                    .enable(true)
                    .select()
                    .apis(RequestHandlerSelectors.basePackage(this::class.java.`package`.name))
                    .paths(PathSelectors.any())
                    .build()
                    .directModelSubstitute(VersionedTypeReference::class.java, String::class.java)
        }
    }
}