# Sample Service Taxonomy Publisher

This sample shows that a simple service exposing bunch of REST end points can make them available to Vyne through a simple
taxi definition. See 'sample-service.taxi' to see how it publishes a service schema.

'sample-service.taxi' is published during the start-up process through the annotations in 'Bootsrapper' class:

```
@VyneSchemaPublisher(schemaFile = "sample-service.taxi")
@EnableConfigurationProperties(VyneSpringHazelcastConfiguration::class)
class Bootstrapper {
```
Please note that this application is configured to use 'EUREKA' based schema distribution mechanism through:

```
vyne:
  schema:
    publicationMethod: EUREKA
```

in application.yml. When you use other schema distribution mechanism, you need to modify the above setting accordingly.


For Swagger UI, goto:
* http://localhost:9234/swagger-ui/index.html
