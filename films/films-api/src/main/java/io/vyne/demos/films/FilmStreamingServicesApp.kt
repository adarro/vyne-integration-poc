package io.vyne.demos.films

import com.fasterxml.jackson.databind.ObjectMapper
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.info.GitProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.kafka.core.KafkaTemplate
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController
import springfox.documentation.builders.RequestHandlerSelectors
import springfox.documentation.spi.DocumentationType
import springfox.documentation.spring.web.plugins.Docket
import springfox.documentation.swagger2.annotations.EnableSwagger2
import java.math.BigDecimal

@SpringBootApplication
@EnableSwagger2
open class FilmStreamingServicesApp {

    companion object {
        val logger = LoggerFactory.getLogger(this::class.java)

        @JvmStatic
        fun main(args: Array<String>) {
            SpringApplication.run(FilmStreamingServicesApp::class.java, *args)
        }
    }

    @Autowired
    fun logInfo(@Autowired(required = false) buildInfo: GitProperties? = null) {
        val commit = buildInfo?.shortCommitId ?: "Unknown"
        logger.info("Films API built on commit $commit")
    }
}

@RestController
class KafkaPublisher(val kafkaTemplate: KafkaTemplate<String, ByteArray>, val objectMapper: ObjectMapper) {
    init {
        logger.info("")
    }
    private val protoLoader = ProtoLoader()
    companion object {
        val logger = LoggerFactory.getLogger(this::class.java)
    }

    data class KafkaRecordMetadata(
        val topic: String,
        val offset: Long,
        val partition: Int,
        val timestamp: Long
    )

    @GetMapping("/proto")
    fun getProtoSpec(): String {
        return protoLoader.protoSpec
    }

    @PostMapping("/kafka/{topic}", consumes = ["*/*"])
    fun publish(@RequestBody content: String, @PathVariable("topic") topic: String): KafkaRecordMetadata {
        val sendResult = kafkaTemplate.send(topic, content.toByteArray()).get()
        return KafkaRecordMetadata(
            sendResult.recordMetadata.topic(),
            sendResult.recordMetadata.offset(),
            sendResult.recordMetadata.partition(),
            sendResult.recordMetadata.timestamp()
        )
    }

    @PostMapping("/kafka/newReleases/{filmId}")
    fun publishNewReleaseAnnouncement(@PathVariable("filmId") filmId: Int): KafkaRecordMetadata {
        val announcement = mapOf(
            "filmId" to filmId,
            "announcement" to "Today, Netflix announced the reboot of yet another classic franchise"
        )
        val protoMessage = protoLoader.protobufSchema.protoAdapter("NewFilmReleaseAnnouncement", false)
            .encode(announcement)
        val sendResult = kafkaTemplate.send(
            "releases",
            protoMessage
        ).get()
        return KafkaRecordMetadata(
            sendResult.recordMetadata.topic(),
            sendResult.recordMetadata.offset(),
            sendResult.recordMetadata.partition(),
            sendResult.recordMetadata.timestamp()
        )
    }
}

@RestController
class StreamingMoviesProvider {
    private val streamingProviders: List<StreamingProvider> = listOf(
        StreamingProvider("Netflix", 9.99.toBigDecimal()),
        StreamingProvider("Disney Plus", 7.99.toBigDecimal()),
        StreamingProvider("Now TV", 13.99.toBigDecimal()),
        StreamingProvider("Hulu", 8.99.toBigDecimal()),
    )

    @GetMapping("/films/{filmId}/streamingProviders")
    fun getStreamingProvidersForFilm(@PathVariable("filmId") filmId: String): List<StreamingProvider> {
        return listOf(streamingProviders.random(), streamingProviders.random()).distinct()
    }
}

@Configuration
open class SwaggerConfig {
    @Bean
    open fun api(): Docket {
        return Docket(DocumentationType.OAS_30)
            .select()
            .apis(RequestHandlerSelectors.basePackage("io.vyne.demos.films"))
            .build()
    }

}


data class StreamingProvider(
    val name: String,
    val pricePerMonth: BigDecimal
)