#!/bin/bash

## requires datahelix to be installed
## https://github.com/finos/datahelix/blob/master/docs/GettingStarted.md

datahelix --max-rows=20 --replace --profile-file=./icap-data-profile.json --output-format=csv --output-path=icap-orders-with-date.csv
