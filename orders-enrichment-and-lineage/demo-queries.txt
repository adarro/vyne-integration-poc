findAll { Order[] } as {
    bloombergOrderId: BloombergOrderId
    isin: Isin
    icapOrderId: IcapOrderId
    cusip: Cusip
    traderId: TraderId
    icapTradeDate: IcapTradeDate
    bloombergTradeDate: BloombergTradeDate
}[]



findAll { Order[] } as {
    orderId: OrderId
    @FirstNotEmpty
    isin: Isin
    @FirstNotEmpty
    cusip: Cusip
}[]



import demo.orderFeeds.users.LastName

findAll { Order[] } as {
    orderId: OrderId
    @FirstNotEmpty
    isin: Isin
    @FirstNotEmpty
    cusip: Cusip
    internalId: InstrumentId
    assetClass: AssetClass
    maturityDate: MaturityDate
    desk: Desk
    lastName: LastName
}[]



findAll { Order[] } as {
    orderId: OrderId
    @FirstNotEmpty
    isin: Isin
    @FirstNotEmpty
    cusip: Cusip
    total: TotalSize
    remainingSize: RemainingSize
    filledSize: FilledSize
}[]



import demo.orderFeeds.users.LastName

findAll { Order[] } as {
    orderId: OrderId
    @FirstNotEmpty
    isin: Isin
    @FirstNotEmpty
    cusip: Cusip
    instrumentId: InstrumentId
    assetClass: AssetClass
    maturityDate: MaturityDate
    traderId: BankTraderId
    desk: Desk
    lastName: LastName
    total: TotalSize
    remainingSize: RemainingSize
    filledSize: FilledSize
}[]

findAll { Order[] } as {
    orderId: OrderId
    @FirstNotEmpty
    isin: Isin
    @FirstNotEmpty
    cusip: Cusip
    instrumentId: InstrumentId
    assetClass: AssetClass
    maturityDate: MaturityDate
    traderId: BankTraderId
    desk: Desk
    lastName: LastName
    total: TotalSize
    remainingSize: RemainingSize
    filledSize: FilledSize
    esgScores :  {
        @FirstNotEmpty
        env: EnvironmentalScore
        @FirstNotEmpty
        soc: SocialScore
        @FirstNotEmpty
        gov: GovernanceScore
        overallEsgScore: Decimal by (EnvironmentalScore + SocialScore + GovernanceScore) / 3
    }
}[]


findAll { Order[] } as AntiMoneyLaunderingReportOrder[]




//
    esgScores: {
        @FirstNotEmpty
        env: EnvironmentalScore
        @FirstNotEmpty
        soc: SocialScore
        @FirstNotEmpty
        gov: GovernanceScore
        overallEsgScore: Decimal by (EnvironmentalScore + SocialScore + GovernanceScore) / 3
    }