package io.vyne.demos.orders.esg

import com.google.common.cache.CacheBuilder
import io.vyne.spring.http.NotFoundException
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RestController
import java.math.BigDecimal
import kotlin.random.Random
import kotlin.random.nextInt


@SpringBootApplication
open class EsgDataServiceApp {
    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            SpringApplication.run(EsgDataServiceApp::class.java, *args)
        }
    }
}


private val msciSupportedIsins = mapOf(
    "FJWWRQW0N1R9" to "7098RV158",
    "VAK617NS3Z15" to "110J5N696",
    "MOCELOY1M4Y9" to "286830T95",
    "LUJWL33K14C7" to "588CHEO67",
    "LA1OVT3IXTW5" to "501E4Q722",
    "MT7V8XU5VOC7" to "89891WWE0",
    "PL4OW3IA1327" to "040JE25C1",
    "EE92AQ5T43R6" to "8452HK1O0",
    "HTJ774VUDVK5" to "854494GJ4",
    "TJ83RSLCD6K4" to "903GTU444",
    "AI82G9AAAU46" to "373FGF238",
    "SNK58UBUA3A4" to "6891DM6Z1",
    "HMU4R021D041" to "5571OP8V8",
    "GB002HGNHT88" to "828I0SL49",
    "SC16A43GV7Z9" to "0271QC391",
    "BVSXB8E2GN94" to "267X3KHK7",
    "ALF9HTNQLVB5" to "257K2LVG9",
    "FR49I3E81067" to "229GR1W87",
    "BA77U6CCWZ57" to "07706VX08",
    "CX8H622BJ9V4" to "478B6R9H8",
    "BF0UQZ547FL2" to "9863A6BJ5",
    "TM8K2K4G52M9" to "705T62104",
    "BS88M489A8R1" to "490N2ONS7",
    "MTI1L0729W68" to "6893U58O8",
    "AZU6RQ85YH95" to "4268981P7",
    "ZM5WMK6G6ZW9" to "671Q1LEN9",
    "AUKTOQ35J434" to "9720X1998",

    )


@RestController
class MsciEsgDataService {
    private val esgScores = CacheBuilder.newBuilder()
        .build<String, MsciEsgData>()

    @GetMapping("/msci/esgScores/{isin}")
    fun loadEsgData(@PathVariable("isin") isin: String): MsciEsgData {
        return if (msciSupportedIsins.containsKey(isin)) {
            esgScores.get(isin) {
                MsciEsgData.buildWithRandomValues(isin)
            }
        } else {
            throw NotFoundException("Isin $isin is not supported by this service.")
        }
    }
}

data class MsciEsgData(
    val isin: String,
    val environmentalScore: BigDecimal,
    val socialScore: BigDecimal,
    val governanceScore: BigDecimal,
) {
    companion object {
        fun buildWithRandomValues(isin: String): MsciEsgData {
            return MsciEsgData(
                isin,
                environmentalScore = randomScore(),
                socialScore = randomScore(),
                governanceScore = randomScore()
            )
        }
    }
}

@RestController
class RefinitivEsgDataService {
    private val assetInfoCache = CacheBuilder.newBuilder()
        .build<String, RefinitivAssetInfo>()

    @GetMapping("/refinitiv/assetInfo/{cusip}")
    fun loadAssetInfo(@PathVariable("cusip") cusip: String): RefinitivAssetInfo {
        return if (msciSupportedIsins.containsValue(cusip)) {
            // It's provided by Msci - use that provider.
            throw NotFoundException("Cusip $cusip is not supported by this service.")
        } else {
            assetInfoCache.get(cusip) {
                RefinitivAssetInfo(
                    cusip,
                    RefinitivEsgScore.buildWithRandomValues()
                )
            }
        }
    }
}

data class RefinitivAssetInfo(
    val cusip: String,
    val esgScores: RefinitivEsgScore
)

data class RefinitivEsgScore(
    val e_score: BigDecimal,
    val s_score: BigDecimal,
    val g_score: BigDecimal
) {
    companion object {
        fun buildWithRandomValues() = RefinitivEsgScore(
            randomScore(),
            randomScore(),
            randomScore()
        )

    }

}


private fun randomScore() = (Random.nextInt(70..99) / 10.0).toBigDecimal()

