



Feeds
* Bloomberg
* Internal

Services
* Company details
* Instrument reference

Enrichments & Transformations
* Convert ISIN to CUSIP & vice versa
* Enrich company name from LEI
* Normalize timestamp
* Derive size fields

Order fields
* ISIN
* CUSIP
* LEI
* Counterparty
* Timestamp
* Status
* Type
* Currency
* TotalSize
* FilledSize
* RemainingSize