#!/bin/bash

## requires datahelix to be installed
## https://github.com/finos/datahelix/blob/master/docs/GettingStarted.md

datahelix --max-rows=100 --replace --profile-file=./test-data-profile.json --output-format=json --output-path=output.json
