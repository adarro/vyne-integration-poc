package io.vyne.demos.ordersEnrichmentAndLineage.client

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.cloud.netflix.eureka.EnableEurekaClient
import org.springframework.core.io.ResourceLoader
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.server.ResponseStatusException

@SpringBootApplication
@EnableEurekaClient
open class ClientServiceApp {
    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            SpringApplication.run(ClientServiceApp::class.java, *args)
        }
    }
}

data class Client(
        val companyName: String,
        val country: String,
        val bankClientId: String,
        val lei: String
)

@RestController
class ClientService(private val resourceLoader: ResourceLoader) {

//    @Value("classpath:test-data/output.json")
//    lateinit var clientDataFile: Resource
    private val clients: List<Client>

    init {
        val clientDataFile = resourceLoader.getResource("classpath:test-data/output.json")
        val mapper = jacksonObjectMapper()
        clients = mapper.readValue(clientDataFile.inputStream)
    }

    private val clientsById = clients.associateBy { it.bankClientId }

    private val clientsByLei = clients.associateBy { it.lei }

    @GetMapping("/clients")
    fun getClients(): List<Client> = clients

    @GetMapping("/client/{id}")
    fun getClient(@PathVariable("id") id: String): Client {
        return clientsById[id] ?: throw ResponseStatusException(HttpStatus.NOT_FOUND)
    }

    @GetMapping("/client/{lei}")
    fun getClientByLei(@PathVariable("lei") lei: String): Client {
        return clientsByLei[lei] ?: throw ResponseStatusException(HttpStatus.NOT_FOUND)
    }
}