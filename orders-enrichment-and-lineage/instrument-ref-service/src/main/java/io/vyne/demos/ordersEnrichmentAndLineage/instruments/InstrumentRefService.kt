package io.vyne.demos.ordersEnrichmentAndLineage.instruments

import com.opencsv.bean.CsvBindByName
import com.opencsv.bean.CsvDate
import com.opencsv.bean.CsvToBean
import com.opencsv.bean.CsvToBeanBuilder
import lang.taxi.generators.kotlin.TypeAliasRegister
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.cloud.netflix.eureka.EnableEurekaClient
import org.springframework.core.io.ResourceLoader
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.server.ResponseStatusException
import java.io.BufferedReader
import java.io.InputStreamReader
import java.time.LocalDate

@SpringBootApplication
@EnableEurekaClient
open class InstrumentServiceApp {
    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            TypeAliasRegister.registerPackage("io.vyne.demos.orders")
            SpringApplication.run(InstrumentServiceApp::class.java, *args)
        }
    }
}

@RestController
class InstrumentService(private val resourceLoader: ResourceLoader) {
    lateinit var instrument: List<Instrument>
    lateinit var instrumentById: Map<String, Instrument>

    init {
        val usersCsvFile = resourceLoader.getResource("classpath:test-data/instrument-data.csv")

        BufferedReader(InputStreamReader(usersCsvFile.inputStream)).use { reader ->

            // create csv bean reader
            val csvToBean: CsvToBean<Instrument> = CsvToBeanBuilder<Instrument>(reader)
                    .withType(Instrument::class.java)
                    .withIgnoreLeadingWhiteSpace(true)
                    .build()

            // convert `CsvToBean` object to list of users
            instrument = csvToBean.parse()
            instrumentById = instrument.associateBy { it.id ?: throw Exception("Id missing for instrument") }
        }
    }

    @GetMapping("/instruments")
    fun getInstruments(): List<Instrument> {
        return instrument
    }

    @GetMapping("/instrument/{id}")
    fun getInstrument(@PathVariable("id") id: String): Instrument {
        return instrumentById[id] ?: throw ResponseStatusException(HttpStatus.NOT_FOUND)
    }

}

enum class AssetClass {
    Fx, Bonds, Equities
}

data class Instrument(
    @CsvBindByName
    var id: String? = null,
    @CsvBindByName
    var assetClass: AssetClass? = null,
    @CsvDate(value = "yyyy-MM-dd")
    @CsvBindByName
    var maturityDate: LocalDate? = null
)