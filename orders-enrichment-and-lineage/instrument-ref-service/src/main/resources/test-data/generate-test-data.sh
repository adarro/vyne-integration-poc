#!/bin/bash

## requires datahelix to be installed
## https://github.com/finos/datahelix/blob/master/docs/GettingStarted.md

datahelix --max-rows=50 --replace --profile-file=./instrument-data-profile.json --output-format=csv --output-path=bloomberg-instrument-data.csv --generation-type=RANDOM
datahelix --max-rows=50 --replace --profile-file=./instrument-data-profile.json --output-format=csv --output-path=icap-instrument-data.csv --generation-type=RANDOM