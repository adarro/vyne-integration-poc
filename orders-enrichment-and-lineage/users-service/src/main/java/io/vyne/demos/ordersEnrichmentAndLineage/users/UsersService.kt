package io.vyne.demos.ordersEnrichmentAndLineage.users

import com.opencsv.bean.CsvBindByName
import com.opencsv.bean.CsvToBean
import com.opencsv.bean.CsvToBeanBuilder
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.cloud.netflix.eureka.EnableEurekaClient
import org.springframework.core.io.ResourceLoader
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.server.ResponseStatusException
import java.io.BufferedReader
import java.io.InputStreamReader


@SpringBootApplication
@EnableEurekaClient
open class UsersApp {
    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            SpringApplication.run(UsersApp::class.java, *args)
        }
    }
}

@RestController
class UsersService(private val resourceLoader: ResourceLoader) {
    lateinit var users: List<Trader>
    lateinit var usersById: Map<String, Trader>

    init {
        val usersCsvFile = resourceLoader.getResource("classpath:test-data/users.csv")

        BufferedReader(InputStreamReader(usersCsvFile.inputStream)).use { reader ->

            // create csv bean reader
            val csvToBean: CsvToBean<Trader> = CsvToBeanBuilder<Trader>(reader)
                    .withType(Trader::class.java)
                    .withIgnoreLeadingWhiteSpace(true)
                    .build()

            // convert `CsvToBean` object to list of users
            users = csvToBean.parse()
            usersById = users.associateBy { it.id ?: throw Exception("Id missing for user") }
        }
    }

    @GetMapping("/traders", produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getOrders(): List<Trader> {
        return users
    }

    @GetMapping("/trader/{id}", produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getOrder(@PathVariable("id") id: String): Trader {
        return usersById[id] ?: throw ResponseStatusException(HttpStatus.NOT_FOUND)
    }
}

enum class Desk {
    Equities,
    FixedIncome,
    Fx,
    Commodities
}

data class Trader(
        @CsvBindByName
        var id: String? = null,
        @CsvBindByName
        var firstName: String? = null,
        @CsvBindByName
        var lastName: String? = null,
        @CsvBindByName
        var desk: Desk? = null
)