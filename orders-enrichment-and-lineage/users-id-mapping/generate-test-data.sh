#!/bin/bash

## requires datahelix to be installed
## https://github.com/finos/datahelix/blob/master/docs/GettingStarted.md

datahelix --max-rows=50 --replace --profile-file=./users-id-mapping-profile.json --output-format=csv --output-path=users-id-mapping.csv
