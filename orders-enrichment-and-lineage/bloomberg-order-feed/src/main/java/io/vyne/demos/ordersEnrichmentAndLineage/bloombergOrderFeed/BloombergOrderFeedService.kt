package io.vyne.demos.ordersEnrichmentAndLineage.bloombergOrderFeed

import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.databind.SerializerProvider
import com.fasterxml.jackson.databind.annotation.JsonSerialize
import com.fasterxml.jackson.databind.ser.std.StdSerializer
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.cloud.netflix.eureka.EnableEurekaClient
import org.springframework.core.io.ResourceLoader
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.server.ResponseStatusException
import java.io.IOException
import java.math.BigDecimal
import java.text.SimpleDateFormat
import java.time.*
import java.time.format.DateTimeFormatter
import java.util.*


@SpringBootApplication
@EnableEurekaClient
open class BloombergOrderFeedApp {
    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            SpringApplication.run(BloombergOrderFeedApp::class.java, *args)
        }
    }
}

@RestController
class BloombergOrderFeedService(private val resourceLoader: ResourceLoader)  {

    private lateinit var orders: List<BloombergOrder>
    private lateinit var ordersById: Map<String, BloombergOrder>

    init {
        val ordersDateFile = resourceLoader.getResource("classpath:test-data/bloomberg-orders-with-date.json")
        val mapper = jacksonObjectMapper()
                .registerModule(JavaTimeModule())
        orders = mapper.readValue(ordersDateFile.inputStream)
        ordersById = orders.associateBy { it.orderId }
    }

    @GetMapping("/orders", produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getOrders(): List<BloombergOrder> {
        return orders
    }

    @GetMapping("/orders/{orderId}", produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getOrder(@PathVariable("orderId") orderId: String): BloombergOrder {
        return ordersById[orderId] ?: throw ResponseStatusException(HttpStatus.NOT_FOUND)
    }
}

enum class OrderStatus {
    New,
    PartiallyFilled,
    Filled,
    Cancelled,
    Expired
}

enum class OrderType {
    Market, Limit, StopLimit, Iceberg
}

data class BloombergOrder(
        val orderId: String,
        // this should be a FIGI instead (BBGID)
        val cusip: String,
        val isin: String? = null,
        @JsonSerialize(using = CustomDateSerializer::class)
        val tradeDate: LocalDate,
        val timestamp: Instant,
        val status: OrderStatus,
        val orderType: OrderType,
        val currency: String,
        val totalSize: BigDecimal,
        val remainingSize: BigDecimal,
        val trader: String
)

class CustomDateSerializer : StdSerializer<LocalDate>(LocalDate::class.java) {
    private val formatter = DateTimeFormatter.ofPattern("dd-MMM-yy")
    override fun serialize(value: LocalDate?, gen: JsonGenerator, arg2: SerializerProvider?) {
        gen.writeString(value?.format(formatter))
    }
}