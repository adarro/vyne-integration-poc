package io.vyne.demos.embeddVyne

import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import kotlinx.coroutines.flow.Flow
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController


@RestController
@Api(value = "Sample Controller", description = "Exposes the entry point to invoke embedded Vyne.")
class SampleController(private val inProcessVyne: InProcessVyne) {
    @ApiOperation(value = "Produce a report using a predefined report definition", produces = "text/csv, ${MediaType.TEXT_PLAIN_VALUE}, application/json")
    @PostMapping(
            "/api/query",
            consumes = [MediaType.APPLICATION_JSON_VALUE, MediaType.TEXT_PLAIN_VALUE, "application/taxiql"],
            produces = [MediaType.APPLICATION_JSON_VALUE]
    )
    suspend fun submitPipeline(@RequestBody request: QueryRequest): Flow<Any?> {
        return inProcessVyne.queryVyne(request.query)
    }
}